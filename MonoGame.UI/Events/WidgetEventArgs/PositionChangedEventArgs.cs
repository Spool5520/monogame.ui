﻿
//===================================================//
// Created by: Max P Snider (Spool) (Spool5520) 2018 //
//===================================================//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame.UI.Events
{
    public class PositionChangedEventArgs : EventArgs
    {
        public int X;
        public int Y;
    }
}
