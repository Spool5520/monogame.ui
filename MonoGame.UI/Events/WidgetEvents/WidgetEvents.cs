﻿
//===================================================//
// Created by: Max P Snider (Spool) (Spool5520) 2018 //
//===================================================//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

using MonoGame.UI.Core;
using MonoGame.UI.Widgets;

namespace MonoGame.UI.Events
{
    public delegate void NameChangedEventHandler(object sender, string newName);

    public delegate void EnableChangedEventHandler(object sender, bool newEnableState);
    public delegate void VisibleChangedEventHandler(Widget sender, bool newVisibleState);

    public delegate void PositionChangedEventHandler(object sender, Point newPosition);
    public delegate void SizeChangedEventHandler(object sender, Point newSize);

    public delegate void InputRectChangedEventHandler(Widget sender, Rectangle newInputRect);

    public delegate void ClickEventHandler();

    public delegate void ChildAddedEventHandler(IContainer sender, Widget newChild);
    public delegate void ChildRemovedEventHandler(IContainer sender, Widget oldChild);
    public delegate void ChildParentChangedEventHandler(IContainer sender, Widget child);
}
