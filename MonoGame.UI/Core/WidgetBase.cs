﻿
//===================================================//
// Created by: Max P Snider (Spool) (Spool5520) 2018 //
//===================================================//

using Microsoft.Xna.Framework;

using MonoGame.UI.Events;
using MonoGame.UI.Widgets; // Added for cref use.

namespace MonoGame.UI.Core
{

    /// <summary>
    /// Base class for all UI <see cref="Widget"/>'s.
    /// </summary>
    /// 
    /// <remarks>
    ///     Provides Id, Name, Enable flag and DestinationRect assignment and change events.
    ///     These values are shared across all Widgets. Regardless if any Widget is renderable or 
    ///     can contain children Widgets or is Mouse interactable. Only add to this class if the 
    ///     values you are adding are not dependent on anything listed above. 
    /// </remarks>
    public abstract class WidgetBase
    {

        #region Name and Id

        /// <summary>
        /// static int shared across all <see cref="Widget"/> this is used to
        /// create a unique Id.
        /// </summary>
        /// 
        /// <remarks>
        ///     Incrimented by 1 everytime it is used to assign <see cref="Id"/>.
        ///     If no Name is provided this is the only means used to generate 
        ///     an Id. Otherwise the Id is generated from the Name hashcode and 
        ///     then the IdCounter value is added to that. 
        /// </remarks>
        private static int _IdCounter = 0;


        /// <summary>
        /// Provides initial empty string for this Widgets Name.
        /// We do this so that we dont have to check for null. 
        /// </summary>
        private string _name = "";


        /// <summary>
        /// Gets the unique Id of this <see cref="Widget"/>.
        /// </summary>
        /// 
        /// <remarks>
        ///     The Set accessor is protected in the case of 
        ///     needing a custom Id generation in a sub class. 
        /// </remarks>
        public int Id { get; protected set; }


        /// <summary>
        /// Gets/Sets the Name of this <see cref="Widget"/>.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;

                // Since we have changed our Name we need to Alert anyone who is effected by this.
                OnNameChange();
            }
        }


        /// <summary>
        /// Event Invoked when the Name of this <see cref="Widget"/> is changed.
        /// </summary>
        public event NameChangedEventHandler NameChange;


        /// <summary>
        /// Method to provide internal logic that may need to be 
        /// done before or after we Invoke <see cref="NameChange"/>.
        /// </summary>
        protected void OnNameChange()
        {
            NameChange?.Invoke(this, _name);
        }

        #endregion

        #region Destination Handling

        private Rectangle _destination;

        public int X
        {
            get { return _destination.X; }
            set
            {
                _destination.X = value;
                OnPositionChange();
            }
        }

        public int Y
        {
            get { return _destination.Y; }
            set
            {
                _destination.Y = value;
                OnPositionChange();
            }
        }

        public int Width
        {
            get { return _destination.Width; }
            set
            {
                _destination.Width = value;
                OnSizeChange();
            }
        }

        public int Height
        {
            get { return _destination.Height; }
            set
            {
                _destination.Height = value;
                OnSizeChange();
            }
        }       

        public Point Position
        {
            get { return _destination.Location; }
            set
            {
                _destination.Location = value;
                OnPositionChange();
            }
        }

        public Point Size
        {
            get { return _destination.Size; }
            set
            {
                _destination.Size = value;
                OnSizeChange();
            }
        }

        public Rectangle DestinationRect
        {
            get { return _destination; }
            set
            {
                _destination = value;
                OnPositionChange();
                OnSizeChange();
            }
        }        

        public event PositionChangedEventHandler PositionChange;

        public event SizeChangedEventHandler SizeChange;

        protected void OnPositionChange()
        {
            PositionChange?.Invoke(this, _destination.Location);
        }

        protected void OnSizeChange()
        {
            SizeChange?.Invoke(this, _destination.Size);
        }

        #endregion

        #region Enable Handling

        private bool _enabled;

        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                _enabled = value;
                OnEnableChange();
            }
        }

        public event EnableChangedEventHandler EnableChange;

        protected void OnEnableChange()
        {
            EnableChange?.Invoke(this, _enabled);
        }

        #endregion

        public WidgetBase(string name = "")
        {
            _name = name;

            if(_name == string.Empty)
            {
                Id = _IdCounter;                
            }
            else
            {
                Id = _name.GetHashCode();
                Id += _IdCounter;
            }

            _IdCounter++;
        }                          
    }
}
