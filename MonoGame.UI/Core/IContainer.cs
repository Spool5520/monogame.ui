﻿
//===================================================//
// Created by: Max P Snider (Spool) (Spool5520) 2018 //
//===================================================//

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MonoGame.UI.Events;
using MonoGame.UI.Widgets;

namespace MonoGame.UI.Core
{
    public interface IContainer
    {
        KeyedCollection<int,Widget> Children { get; set; }

        void AddChild(Widget child);
        void RemoveChild(int childId);
        void RemoveChild(string childName);

        T GetChildById<T>(int childId) where T : Widget;
        T GetChildByName<T>(string childName) where T : Widget;

        List<Widget> GetAllChildren();
        List<T> GetAllChildrenOfType<T>() where T : Widget;
        List<Widget> GetAllEnabledChildren();
        List<Widget> GetAllVisibleChildren();
        List<IContainer> GetAllChildrenContainers();

        event ChildAddedEventHandler ChildAdded;
        event ChildRemovedEventHandler ChildRemoved;
        event ChildParentChangedEventHandler ChildParentChange;
    }
}
