﻿
//===================================================//
// Created by: Max P Snider (Spool) (Spool5520) 2018 //
//===================================================//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using MonoGame.UI.Core;
using MonoGame.UI.Events;

namespace MonoGame.UI.Widgets
{
    public class Widget : WidgetBase
    {
        #region InputRect Handling

        private Rectangle _inputRect;

        public Rectangle InputRect
        {
            get { return _inputRect; }
            set
            {
                _inputRect = value;
                OnInputRectChange();
            }
        }

        public event InputRectChangedEventHandler InputRectChange;

        protected void OnInputRectChange()
        {
            InputRectChange?.Invoke(this, _inputRect);
        }

        #endregion

        #region Mouse Input Handling

        public event ClickEventHandler Click;
        public event ClickEventHandler LeftClick;
        public event ClickEventHandler RightClick;
        public event ClickEventHandler MiddleClick;

        protected void OnClick()
        {
            Click?.Invoke();
        }

        protected void OnLeftClick()
        {
            LeftClick?.Invoke();
        }

        protected void OnRightClick()
        {
            RightClick?.Invoke();
        }

        protected void OnMiddleClick()
        {
            MiddleClick?.Invoke();
        }

        #endregion

        #region Render Region

        private bool _visible;

        public bool Visible
        {
            get { return _visible; }
            set
            {
                _visible = value;
                OnVisibleChange();
            }
        }

        public event VisibleChangedEventHandler VisibleChange;

        protected void OnVisibleChange()
        {
            VisibleChange?.Invoke(this, _visible);
        }

        public Texture2D Texture { get; set; }

        public Color DrawColor { get; set; } = Color.White;
        public Color HoverColor { get; set; }
        public Color ClickColor { get; set; }

        #endregion

        public IContainer Parent { get; set; }

        public Widget(string name = "") : base(name) { }      
    }
}
