﻿
//===================================================//
// Created by: Max P Snider (Spool) (Spool5520) 2018 //
//===================================================//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame.UI.Input
{
    public enum MouseButton
    {
        Left,
        Right,
        Middle
    }
}
