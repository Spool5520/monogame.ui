﻿
//===================================================//
// Created by: Max P Snider (Spool) (Spool5520) 2018 //
//===================================================//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using MonoGame.UI.Widgets;

namespace MonoGame.UI.Managers
{   

    public class InputManager
    {
        public MouseState CurrentMouseState { get; private set; }
        public MouseState PreviousMouseState { get; private set; }

        public KeyboardState CurrentKeyboardState { get; private set; }
        public KeyboardState PreviousKeyboardState { get; private set; }

        public Point CurrentMouseLocation { get { return CurrentMouseState.Position; } }

        public InputManager()
        {
            
        }

        public void Update(GameTime gameTime)
        {
            PreviousMouseState = CurrentMouseState;
            CurrentMouseState = Mouse.GetState();


        }
    }
}
