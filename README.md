# MonoGame.UI

MonoGame.UI is an OpenSource UI framework for MonoGame Projects.

See [Wiki](https://bitbucket.org/Spool5520/monogame.ui/wiki/Home) for [Tutorials](https://bitbucket.org/Spool5520/monogame.ui/wiki/Home), [Documentation](https://bitbucket.org/Spool5520/monogame.ui/wiki/Documentation) and [API Reference](https://bitbucket.org/Spool5520/monogame.ui/wiki/API%20Reference).

## Goals
- Fully featured modern UI
    - Provide extensive toolbox of common UI Widgets: Button, Panel, Window, Textbox ect.
    - Provide Widget Layout capabilites: Grid, Horizontal, Vertical ect.
    - Provide ability to create UI from markup language:
        - Xml, Json, Txt, Lua
    - Provide Visual Editor to make UI creation more productive, Editor not requied to make UI.
    - Provide StyleSheet system for customizing the look of UI.



- Intuitive API design
    - API should be designed with ease of use for the end user as the highest priority.
    - API should be clear understandable and follow Microsoft [Framework Design Guidelines](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/)



- Internal Framework 
    - Internal Framework should be created with performance as the highest priority.
    - Performance should not effect API ease of use. 
    - Offloading drawbacks of performance first design to end user is strictly unacceptable.